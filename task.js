//Числа
numberReal = 1.5
numberInteger = 2
numberInteger++
result_1 = numberReal**3 + numberInteger/2
console.log('Вещественное число:',numberReal,'\nЦелое число:',numberInteger, '\nРезультат:', result_1)
//Строки
str_1 = 'stroka_1'
str_2 = 'stroka_2'
str_3 = str_1+ ' ' + str_2 + ' ' + 'stroka_3'
len = str_3.length
console.log('\nСтрока_1:', str_1, '\nСтрока_2:', str_2, '\nСтрока_3:', str_3, '\nРазмер строки_3:', len)
//Логическое значкение
bool = true
if(bool){
    console.log('\nЛогическое значение:',bool)
}
//Массив
mas_1 = [numberReal, numberInteger, str_1, bool]
mas_1.pop()
mas_2 = mas_1.toString()
mas_3 = ['d','g','q','a'].sort()
console.log('\nМассив_1:', mas_1, '\nМассив_2:', mas_2, '\nМассив_3:', mas_3)
//Объект
obj = {
    one: 1,
    two: 1.2,
    three: function(){
        return this.one + this.two
    }
}
console.log('\nОбъект:',obj, '\nМетод объекта: ',obj.three())
